# ESP_AutoWifi

#### 介绍
一个用于esp32 和esp8266 wifi 自动配网的库，支持web配网

支持强制门户，手机连接热点后即跳出配网页面


简单使用方法，只需几行代码即可完成配网

#### 代码示例

```
#include<ESP_AutoWifi.h>

AutoWifi wifi_auto;//初始化一个自动配网对象

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  wifi_auto.begin();//开始自动配网,一直等待wifi连接成功才返回 阻塞

  //wifi_auto.begin(60);//增加一个时间参数 单位为秒，定时在60秒内 wifi无论是否连接成功，都返回 非阻塞
}

void loop() {

```
#### 运行结果

这是示例二 TFT_eSPI_Connect_demo.ino 连接失败后显示的画面
![输入图片说明](4EE6324E0D805149405C51E0517EA391.jpg)

这是配网页面，连接热点后会自动弹出
![输入图片说明](74DAC34B66A197ADC59530FE00783ED6.jpg)

